import {Routes} from '@angular/router';
import {UsersComponent} from "./main/pages/users/users.component";
import {BooksComponent} from "./main/pages/books/books.component";
import {SettingsComponent} from "./main/pages/settings/settings.component";
import {SalesComponent} from "./main/pages/sales/sales.component";

export const routes: Routes = [
  {
    path: 'users', component: UsersComponent,
  },
  {
    path: 'settings', component: SettingsComponent,
  },
  {
    path: 'books', component: BooksComponent
  },
  {
    path: 'sales', component: SalesComponent
  }
];
