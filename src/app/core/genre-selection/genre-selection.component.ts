import {Component, OnInit} from '@angular/core';
import {AutoCompleteModule} from "primeng/autocomplete";
import {GenreModel} from "../../model/genre.model";
import {DropdownModule} from "primeng/dropdown";
import {ConfigService} from "../../services/config.service";
import {FormsModule} from "@angular/forms";
import {MultiSelectModule} from "primeng/multiselect";

@Component({
  selector: 'genre-selection',
  standalone: true,
  imports: [
    AutoCompleteModule,
    DropdownModule,
    FormsModule,
    MultiSelectModule
  ],
  templateUrl: './genre-selection.component.html',
  styleUrl: './genre-selection.component.scss'
})
export class GenreSelectionComponent implements OnInit {

  value: GenreModel;

  genres: GenreModel[];

  selectedGenres: number[];

  constructor(private configService: ConfigService) {
  }

  ngOnInit(): void {
    this.configService.genres$
      .subscribe(genres => {
        this.genres = genres;
      })
  }

}
