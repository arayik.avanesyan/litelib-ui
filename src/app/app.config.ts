import {APP_INITIALIZER, ApplicationConfig} from '@angular/core';
import {provideRouter, RouterLink} from '@angular/router';

import {routes} from './app.routes';
import {HTTP_INTERCEPTORS, HttpClientModule, provideHttpClient, withFetch} from "@angular/common/http";
import {KeycloakAngularModule, KeycloakBearerInterceptor, KeycloakService} from "keycloak-angular";
import {CommonModule} from "@angular/common";
import {KeycloakConfig} from "keycloak-js";
import {RequestService} from "./services/request.service";
import {LlKeycloakService} from "./services/ll-keycloak.service";
import {AuthGuardService} from "./services/auth-guard.service";
import {UserApi} from "./api/endpoints/user.api";
import {TableModule} from "primeng/table";
import {BookApi} from "./api/endpoints/book.api";
import {provideAnimations} from "@angular/platform-browser/animations";
import {MessageService} from "primeng/api";
import {environment} from "../environments/environment";
import {ConfigService} from "./services/config.service";
import {GenreApi} from "./api/endpoints/genre.api";
import {UserPreferenceApi} from "./api/endpoints/user-preference.api";
import {BookPurchaseHistoryApi} from "./api/endpoints/book-purchase-history-api.service";

function preloadData(configService: ConfigService): Promise<boolean> {
  return configService.preloadGlobalData();
}

export function initializer(keycloakService: LlKeycloakService,
                            configService: ConfigService): () => Promise<any> {

  const {hostname} = new URL(window.location.href);
  let keycloakConfig: KeycloakConfig = {
    url: environment.kcUrl,
    realm: 'litelib',
    clientId: 'litelib-ui'
  }

  return (): Promise<any> => {
    return keycloakService
      .initializeKeycloak(keycloakConfig)
      .then(() => keycloakService.checkIfTokenRefreshRequired())
      .then(() => preloadData(configService));
  };
}


export const appConfig: ApplicationConfig = {
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [LlKeycloakService, ConfigService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: KeycloakBearerInterceptor,
      multi: true
    },
    provideRouter(routes),
    provideAnimations(),
    provideHttpClient(withFetch()),
    UserApi,
    GenreApi,
    BookPurchaseHistoryApi,
    UserPreferenceApi,
    BookApi,
    CommonModule,
    MessageService,
    RequestService,
    AuthGuardService,
    HttpClientModule,
    KeycloakAngularModule,
    TableModule,
    LlKeycloakService,
    ConfigService,
    RouterLink,
    KeycloakService
  ]
};
