import {AuthorModel} from "../../model/author.model";

export class BookPurchaseHistoryModel {
  bookId: number;
  title: string;
  authors: AuthorModel[]
  quantity: number;
}
