export interface PageQuery {
  size?: number;
  number?: number;
}
