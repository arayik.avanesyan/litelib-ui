export interface ApiResponse<T> {
  data: T;
  errors: ErrorModel[];
  meta: Meta;
}

export interface Meta {
  page: number;
  size: number;
  totalElements: number;
}

export interface ErrorModel {
  message: string;
}
