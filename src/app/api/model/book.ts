import {AuthorModel} from "../../model/author.model";
import {GenreModel} from "../../model/genre.model";

export class Book {
  id: number;
  title: string;
  authors: AuthorModel[];
  genres: GenreModel[];
  description: string;
  isbn: string;
  image: string;
  published: string;
  publisher: string;
  inStock: number;
}
