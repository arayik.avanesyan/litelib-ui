import {Observable} from 'rxjs';

import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {CommunicationService} from "../../services/communication.service";
import {BookPurchaseHistoryModel} from "../model/book-purchase-history.model";

@Injectable()
export class BookPurchaseHistoryApi extends CommunicationService {

  protected override apiEndpoint = this.env.apiEndpoint + 'v1/books/purchase/history';

  findAll(): Observable<BookPurchaseHistoryModel[]> {
    const url = this.apiEndpoint;
    return this.requestService
      .get(url)
      .pipe(
        map(response => response.data as BookPurchaseHistoryModel[]),
        catchError(this.handleError)
      );
  }

}
