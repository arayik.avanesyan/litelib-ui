import {Observable} from 'rxjs';

import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {CommunicationService} from "../../services/communication.service";
import {User, UserEditModel} from "../model/user";

@Injectable()
export class UserApi extends CommunicationService {

  protected override apiEndpoint = this.env.apiEndpoint + 'v1/users';

  findAll(): Observable<User[]> {
    const url = this.apiEndpoint;
    console.log('url', url);
    return this.requestService
      .get(url)
      .pipe(
        map(response => response.data as User[]),
        catchError(this.handleError)
      );
  }

  create(user: UserEditModel): Observable<void> {
    const url = this.apiEndpoint;
    return this.requestService
      .post(url, user)
      .pipe(
        catchError(this.handleError)
      );
  }

  uploadCsv(file: File): Observable<any> {
    const url = `${this.apiEndpoint}/import`;
    return this.requestService.upload(url, file)
      .pipe(
        catchError(this.handleError)
      );
  }

}
