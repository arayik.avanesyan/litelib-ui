import {Injectable} from "@angular/core";
import {CommunicationService} from "../../services/communication.service";
import {Observable} from "rxjs";
import {ApiResponse} from "../model/api.response";
import {catchError} from "rxjs/operators";
import {GenreModel} from "../../model/genre.model";

@Injectable()
export class GenreApi extends CommunicationService {

  protected override apiEndpoint = this.env.apiEndpoint + 'v1/genres';

  findAll(): Observable<ApiResponse<GenreModel[]>> {
    const url = `${this.apiEndpoint}`;
    return this.requestService
      .get(url)
      .pipe(
        catchError(this.handleError)
      );
  }
}
