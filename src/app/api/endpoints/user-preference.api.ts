import {Observable} from 'rxjs';

import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {CommunicationService} from "../../services/communication.service";

@Injectable()
export class UserPreferenceApi extends CommunicationService {

  protected override apiEndpoint = this.env.apiEndpoint + 'v1/preferences';

  update(genreIds: number[]): Observable<void> {
    const url = `${this.apiEndpoint}`;
    return this.requestService
      .put(url, {
        genreIds: genreIds,
      })
      .pipe(
        catchError(this.handleError)
      );
  }

}
