import {Observable} from 'rxjs';

import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {CommunicationService} from "../../services/communication.service";
import {Book} from "../model/book";
import {ApiResponse} from "../model/api.response";
import {PageQuery} from "../model/page-query";

@Injectable()
export class BookApi extends CommunicationService {

  protected override apiEndpoint = this.env.apiEndpoint + 'v1/books';

  findAll(pageQuery: PageQuery): Observable<ApiResponse<Book[]>> {
    const page: number = pageQuery.number;
    const size: number = pageQuery.size;
    const url = `${this.apiEndpoint}?page=${page}&size=${size}`;
    return this.requestService
      .get(url)
      .pipe(
        catchError(this.handleError)
      );
  }

  getPreferences(): Observable<ApiResponse<Book[]>> {
    const url = `${this.apiEndpoint}/preferences`;
    return this.requestService
      .get(url)
      .pipe(
        catchError(this.handleError)
      );
  }

  get(bookId: number): Observable<ApiResponse<Book>> {
    const url = `${this.apiEndpoint}/${bookId}`;
    return this.requestService
      .get(url)
      .pipe(
        catchError(this.handleError)
      );
  }

  purchase(bookId: number, quantity: number): Observable<void> {
    const url = `${this.apiEndpoint}/${bookId}/purchase`;
    return this.requestService
      .post(url, {
        quantity: quantity
      })
      .pipe(
        catchError(this.handleError)
      );
  }
}
