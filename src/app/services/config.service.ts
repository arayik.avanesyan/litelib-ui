import {Injectable} from "@angular/core";
import {BehaviorSubject, zip} from "rxjs";
import {GenreModel} from "../model/genre.model";
import {GenreApi} from "../api/endpoints/genre.api";

@Injectable({providedIn: 'root'})
export class ConfigService {

  public readonly genres$: BehaviorSubject<GenreModel[]> = new BehaviorSubject<GenreModel[]>([]);

  constructor(private genreApi: GenreApi) {
  }

  public preloadGlobalData(): Promise<boolean> {

    const loadGenres = () => {
      return this.genreApi.findAll();
    }

    return new Promise((resolve, reject) => {
      zip(
        loadGenres(),
      ).subscribe(([genres]) => {
        this.genres$.next(genres.data as GenreModel[]);
        resolve(true);
      }, (err) => {
        console.log(err);
        reject(err);
      });
    });


  }

}
