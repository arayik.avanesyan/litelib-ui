import {Injectable} from '@angular/core';
import {from, mergeMap, Observable} from 'rxjs';
import {HttpClient} from "@angular/common/http";
import {KeycloakService} from "keycloak-angular";

@Injectable()
export class RequestService {

  constructor(private http: HttpClient, private keycloakService: KeycloakService) {
  }

  async createAuthorizationHeader() {
    const token = await this.keycloakService.getToken();
    return {
      Authorization: 'Bearer ' + token
    }
  }

  get(url: string, options?: any): Observable<any> {
    return from(this.createAuthorizationHeader()).pipe(
      mergeMap(headers =>
        this.http.get(url, {...options, headers})
      )
    );
  }

  post(url: string, body?: any, options?: any): Observable<any> {
    return from(this.createAuthorizationHeader()).pipe(
      mergeMap(headers =>
        this.http.post(url, body, {...options, headers})
      )
    );
  }

  upload(url: string, file: File, options?: any): Observable<any> {
    return from(this.createAuthorizationHeader()).pipe(
      mergeMap(headers => {
        const formData: FormData = new FormData();
        formData.append('file', file);
        return this.http.post(url, formData, {...options, headers});
      })
    );
  }

  put(url: string, body: any, options?: any): Observable<any> {
    return from(this.createAuthorizationHeader()).pipe(
      mergeMap(headers =>
        this.http.put(url, body, {...options, headers})
      )
    );
  }
}
