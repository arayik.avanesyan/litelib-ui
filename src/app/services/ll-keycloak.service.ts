import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {KeycloakService} from 'keycloak-angular';
import {KeycloakConfig, KeycloakProfile, KeycloakTokenParsed} from 'keycloak-js';
import {UserRoles} from "../api/model/user.roles";
import {LoggedInUser} from "../model/logged-in-user";

@Injectable({
  providedIn: 'root',
})
export class LlKeycloakService {
  user = new LoggedInUser();

  constructor(private keycloakService: KeycloakService) {
  }

  initializeKeycloak(keycloakConfig: KeycloakConfig) {
    console.log('initializeKeycloak');
    return this.keycloakService.init({
      config: keycloakConfig,
      initOptions: {
        onLoad: 'login-required',
        checkLoginIframe: false,
      },
      enableBearerInterceptor: true,
      bearerExcludedUrls: ['/assets'],
    });
  }

  getUser(): Observable<LoggedInUser> {
    return Observable.create((observer: any) => {
      this.keycloakService
        .loadUserProfile()
        .then((fetchedUser: KeycloakProfile) => {
          console.log('fetchedUser', this.user);
          let userRoles = this.keycloakService.getUserRoles(false, 'litelib-ui');
          this.user = {roles: userRoles, ...fetchedUser};
          console.log('this.user', this.user);
        });

      observer.next(this.user);
      observer.complete();
    });
  }

  logout() {
    this.keycloakService.getKeycloakInstance().logout().then(value => {
      console.log('Logged out !!!')
    });
  }

  checkIfTokenRefreshRequired() {
    return this.keycloakService.isLoggedIn();
  }

  hasRole(role: string | UserRoles): boolean {
    const userRoles = this.keycloakService.getUserRoles(true);
    if (userRoles && userRoles.length === 0) {
      return false;
    }
    return userRoles.indexOf(role.toString()) >= 0;
  }
}
