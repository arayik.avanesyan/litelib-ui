import {throwError as observableThrowError} from 'rxjs';
import {Injectable} from '@angular/core';
import {RequestService} from './request.service';
import {environment} from "../../environments/environment";

@Injectable()
export class CommunicationService {
  protected apiEndpoint: string | undefined;
  protected env: any;

  constructor(protected requestService: RequestService) {
    this.env = environment;
  }

  protected handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    console.log('handleError error', error);
    const errMsg = error.message
      ? error.message
      : error.status
        ? `${error.status} - ${error.statusText}`
        : 'Server error';
    switch (error.status) {
      case 400:
      case 405:
      case 409:
        return observableThrowError(error.error);
      case 401:
        break;
      case 403:
        return observableThrowError('No access');
      case 422:
        return observableThrowError('Can not process file');
      case 423:
        return observableThrowError('Unable to process');
    }
    return observableThrowError(errMsg);
  }
}
