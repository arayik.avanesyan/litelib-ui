import {Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {LlKeycloakService} from "../services/ll-keycloak.service";

@Directive({
  standalone: true,
  selector: '[hasRole]'
})
export class HasRoleDirective implements OnInit, OnDestroy {

  @Input() hasRole: string;

  isVisible = false;

  constructor(
    private viewContainerRef: ViewContainerRef,
    private templateRef: TemplateRef<any>,
    private keycloakService: LlKeycloakService
  ) {
  }

  ngOnInit(): void {

    const makeItVisible = () => {
      this.isVisible = true;
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    };

    if (this.hasRole && this.hasRole.trim() !== '') {
      const hasAccess = this.keycloakService.hasRole(this.hasRole);
      if (hasAccess) {
        makeItVisible();
      } else {
        this.isVisible = false;
        this.viewContainerRef.clear();
      }
    } else {
      makeItVisible();
    }
  }

  ngOnDestroy(): void {
  }

}
