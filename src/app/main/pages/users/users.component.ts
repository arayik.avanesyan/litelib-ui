import {Component, OnInit} from '@angular/core';
import {User, UserEditModel} from "../../../api/model/user";
import {UserApi} from "../../../api/endpoints/user.api";
import {BehaviorSubject} from "rxjs";
import {AsyncPipe, NgIf} from "@angular/common";
import {MessageService, SharedModule} from "primeng/api";
import {TableModule} from "primeng/table";
import {ButtonModule} from "primeng/button";
import {HasRoleDirective} from "../../../directives/has-role.directive";
import {LlKeycloakService} from "../../../services/ll-keycloak.service";
import {UserRoles} from "../../../api/model/user.roles";
import {DialogModule} from "primeng/dialog";
import {InputTextModule} from "primeng/inputtext";
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {FileUploadEvent, FileUploadModule} from "primeng/fileupload";

@Component({
  selector: 'll-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  imports: [
    AsyncPipe,
    SharedModule,
    TableModule,
    ButtonModule,
    HasRoleDirective,
    NgIf,
    DialogModule,
    InputTextModule,
    ReactiveFormsModule,
    FileUploadModule
  ],
  standalone: true
})
export class UsersComponent implements OnInit {


  users$: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);

  userCreationWindowVisibility = false;

  formGroup: FormGroup;

  user: UserEditModel;

  constructor(private api: UserApi,
              private fb: FormBuilder,
              private messageService: MessageService,
              private llKeycloakService: LlKeycloakService) {
  }

  ngOnInit() {
    this.loadUsers()
  }

  private loadUsers() {
    this.api.findAll().subscribe((users: User[]) => {
        this.users$.next(users);
      }, (err) => {
      },
      () => {
      }
    );
  }

  hasAccess() {
    return this.llKeycloakService.hasRole(UserRoles.ADMIN) ||
      this.llKeycloakService.hasRole(UserRoles.SUPER_ADMIN);
  }

  createUser() {
    if (this.formGroup.valid) {

      this.api.create(this.formGroup.value as UserEditModel).subscribe(
        value => {
          this.messageService.add({severity: 'info', detail: 'User has been created'})
        },
        errorsResponse => {
          this.messageService.add({
            severity: 'error',
            detail: 'User was not created: ' + errorsResponse.errors[0].message
          })
        }, () => {
          this.loadUsers();
        })
    } else {
      this.formGroup.markAsTouched();
    }
  }

  getTitle(): string {
    if (this.llKeycloakService.hasRole(UserRoles.ADMIN)) {
      return 'Create user'
    }
    return 'Create admin';
  }

  hideUserCreationWindow() {
    this.userCreationWindowVisibility = false;
  }

  showUserCreationWindow() {
    this.user = new UserEditModel();
    this.formGroup = this.fb.group({
      firstName: new FormControl(this.user.firstName, Validators.max(50)),
      lastName: new FormControl(this.user.lastName, Validators.max(50)),
      email: new FormControl(this.user.email, Validators.email),
      username: new FormControl(this.user.username, Validators.max(50)),
      password: new FormControl(this.user.password, Validators.required),
    })
    this.userCreationWindowVisibility = true;
  }

  onUpload($event: FileUploadEvent | any) {
    this.api.uploadCsv($event.files[0]).subscribe(
      value => {
        this.messageService.add({severity: 'info', detail: 'Successfully imported'})
      }, error => {
        this.messageService.add({severity: 'warn', detail: 'Something went wrong: '})
      }, () => {
        this.loadUsers();
      });
  }

  hasUserCreationAccess(): boolean {
    return this.llKeycloakService.hasRole(UserRoles.SUPER_ADMIN) || this.llKeycloakService.hasRole(UserRoles.ADMIN)
  }
}
