import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {BookApi} from "../../../api/endpoints/book.api";
import {Book} from "../../../api/model/book";
import {PaginatorModule, PaginatorState} from "primeng/paginator";
import {ApiResponse} from "../../../api/model/api.response";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {PageQuery} from "../../../api/model/page-query";
import {AsyncPipe, DatePipe, NgIf} from "@angular/common";
import {ButtonModule} from "primeng/button";
import {DialogModule} from "primeng/dialog";
import {MessageService, SharedModule} from "primeng/api";
import {TableModule} from "primeng/table";
import {map} from "rxjs/operators";
import {ChipsModule} from "primeng/chips";
import {AuthorModel} from "../../../model/author.model";
import {GenreModel} from "../../../model/genre.model";

@Component({
  selector: 'app-books',
  standalone: true,
  imports: [
    AsyncPipe,
    ButtonModule,
    DatePipe,
    DialogModule,
    PaginatorModule,
    SharedModule,
    TableModule,
    NgIf,
    ChipsModule
  ],
  templateUrl: './books.component.html',
  styleUrl: './books.component.scss'
})
export class BooksComponent implements OnInit {

  books$: BehaviorSubject<Book[]> = new BehaviorSubject<Book[]>([]);

  total$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  showPurchaseModal = false;

  purchaseBook: Book = undefined;

  pageQuery: PageQuery = {
    size: 20,
    number: 0
  };


  constructor(private api: BookApi,
              private router: Router,
              private route: ActivatedRoute,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      this.pageQuery.number = params['page'] || 0;
      this.loadBooks();
    });
  }

  onPageChange($event: PaginatorState) {
    this.router.navigate([], {queryParams: {page: $event.page}, queryParamsHandling: 'merge'});
  }

  preview(id: number) {
    this.getBook(id).subscribe(value => {
      this.purchaseBook = value;
      this.showPurchaseModal = true;
    });
  }

  hidePurchaseModal() {
    this.showPurchaseModal = false;
    this.purchaseBook = undefined;
  }

  private loadBooks() {
    this.api.getPreferences().subscribe((response: ApiResponse<Book[]>) => {
        this.books$.next(response.data);
        this.total$.next(response.meta?.totalElements);
      }
    );
  }

  private getBook(id: number): Observable<Book> {
    return this.api.get(id).pipe(map(it => it.data));
  }

  purchase(purchaseBook: Book, quantity: number) {
    this.api.purchase(purchaseBook.id, quantity).subscribe(
      value => {
        this.messageService.add({severity: 'info', detail: 'Book has been successfully purchased'})
        this.loadBooks();
      },
      error => {
        this.messageService.add({severity: 'error', detail: 'Purchase was failed: ' + error})
      },
      () => {
        this.hidePurchaseModal();
      }
    )
  }

  getAuthors(authors: AuthorModel[]) {
    return authors.map(it => it.firstName + ' ' + it.lastName).join(',');
  }

  getGenres(genres: GenreModel[]) {
    return genres.map(it => it.name).join(',');
  }
}
