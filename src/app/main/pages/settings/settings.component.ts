import {Component, OnInit} from '@angular/core';
import {PaginatorModule} from "primeng/paginator";
import {AsyncPipe, DatePipe, NgIf} from "@angular/common";
import {ButtonModule} from "primeng/button";
import {DialogModule} from "primeng/dialog";
import {MessageService, SharedModule} from "primeng/api";
import {TableModule} from "primeng/table";
import {ChipsModule} from "primeng/chips";
import {GenreSelectionComponent} from "../../../core/genre-selection/genre-selection.component";
import {UserPreferenceApi} from "../../../api/endpoints/user-preference.api";

@Component({
  selector: 'app-books',
  standalone: true,
  imports: [
    AsyncPipe,
    ButtonModule,
    DatePipe,
    DialogModule,
    PaginatorModule,
    SharedModule,
    TableModule,
    NgIf,
    ChipsModule,
    GenreSelectionComponent
  ],
  templateUrl: './settings.component.html',
  styleUrl: './settings.component.scss'
})
export class SettingsComponent implements OnInit {

  constructor(private userPreferenceApi: UserPreferenceApi,
              private messageService: MessageService) {
  }

  ngOnInit() {
  }

  savePreference(genresIds: number[]) {
    this.userPreferenceApi.update(genresIds).subscribe(value => {
      this.messageService.add({severity: 'success', detail: 'Preferences has been updated'})
    })
    console.log('genresIds', genresIds);
  }
}
