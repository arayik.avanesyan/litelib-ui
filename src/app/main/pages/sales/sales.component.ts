import {Component, OnInit} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {AuthorModel} from "../../../model/author.model";
import {GenreModel} from "../../../model/genre.model";
import {BookPurchaseHistoryModel} from "../../../api/model/book-purchase-history.model";
import {BookPurchaseHistoryApi} from "../../../api/endpoints/book-purchase-history-api.service";
import {TableModule} from "primeng/table";
import {AsyncPipe} from "@angular/common";

@Component({
  selector: 'app-sales',
  standalone: true,
  imports: [
    TableModule,
    AsyncPipe
  ],
  templateUrl: './sales.component.html',
  styleUrl: './sales.component.scss'
})
export class SalesComponent implements OnInit {

  bookPurchaseHistory$: BehaviorSubject<BookPurchaseHistoryModel[]> = new BehaviorSubject<BookPurchaseHistoryModel[]>([]);

  constructor(private api: BookPurchaseHistoryApi) {
  }

  ngOnInit() {
    this.loadHistory()
  }

  private loadHistory() {
    this.api.findAll().subscribe((historyModels: BookPurchaseHistoryModel[]) => {
        this.bookPurchaseHistory$.next(historyModels);
      }
    );
  }

  getAuthors(authors: AuthorModel[]) {
    return authors.map(it => it.firstName + ' ' + it.lastName).join(',');
  }

  getGenres(genres: GenreModel[]) {
    return genres.map(it => it.name).join(',');
  }
}
