import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterLink, RouterOutlet} from '@angular/router';
import {ToolbarModule} from "primeng/toolbar";
import {ButtonModule} from "primeng/button";
import {TabMenuModule} from "primeng/tabmenu";
import {MenuItem} from "primeng/api";
import {BadgeModule} from "primeng/badge";
import {SplitButtonModule} from "primeng/splitbutton";
import {InputTextModule} from "primeng/inputtext";
import {LoggedInUser} from "./model/logged-in-user";
import {LlKeycloakService} from "./services/ll-keycloak.service";
import {StyleClassModule} from "primeng/styleclass";
import {RippleModule} from "primeng/ripple";
import {UserRoles} from "./api/model/user.roles";
import {ToastModule} from "primeng/toast";
import {GenreSelectionComponent} from "./core/genre-selection/genre-selection.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterLink, RouterOutlet, ToolbarModule, ButtonModule, TabMenuModule, BadgeModule, SplitButtonModule, InputTextModule, StyleClassModule, RippleModule, ToastModule, GenreSelectionComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
  title = 'litelib-ui';

  user: LoggedInUser;

  items: MenuItem[];

  constructor(private keycloakService: LlKeycloakService) {
  }

  ngOnInit(): void {
    this.items = [
      {
        id: 'users',
        label: 'Users',
        icon: 'user',
        url: '/users',
        visible: this.keycloakService.hasRole(UserRoles.ADMIN) || this.keycloakService.hasRole(UserRoles.SUPER_ADMIN),
      },
      {
        id: 'books',
        label: 'Books',
        icon: 'book',
        url: '/books',
        visible: this.keycloakService.hasRole(UserRoles.USER),
      },
      {
        id: 'sales',
        label: 'Sales',
        icon: 'stats',
        url: '/sales',
        visible: this.keycloakService.hasRole(UserRoles.SUPER_ADMIN),
      },
      {
        id: 'sales',
        label: 'Settings',
        icon: 'settings',
        url: '/settings',
        visible: this.keycloakService.hasRole(UserRoles.USER),
      }

    ];
  }

  logout() {
    this.keycloakService.logout();
  }

  getRole(): string {
    if (this.keycloakService.hasRole(UserRoles.USER)) {
      return 'User';
    } else if (this.keycloakService.hasRole(UserRoles.ADMIN)) {
      return 'Admin';
    } else {
      return 'Super admin';
    }
  }
}
